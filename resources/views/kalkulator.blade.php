<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Kalkulator Zakat</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<!-- Custom CSS -->

	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet" />

	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-180x180.png" />
	<style type="text/css">
		.datepicker {
			font-size: 0.875em;
		}

		.datepicker td,
		.datepicker th {
			width: 1.5em;
			height: 1.5em;
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="home">
			<div class="text-center">
				<a href="https://zakatsukses.org/"><img src="{{asset('assets/img/logo.png')}}" class="image-logo" width="150px"></a>
			</div>
			<div class="text-center mt-4">
				<p class="judul-form">Kalkulator Perhitungan Zakat</p>
			</div>

			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-penghasilan">
							<div class="card-1">
								<i class="fas fa-wallet fa-3x" style="color: #FFA825; margin-left: 16px; margin-top: 12px;"></i>
								<p class="z-penghasilan" style="text-decoration:none">Zakat Penghasilan</p>
								<p class="z-des-penghasilan">Zakat harta yang dikeluarkan dari hasil pendapatan seseorang atau profesinya bila telah mencapai nisab</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-tabungan">
							<div class="card-5">
								<div class="clearfix">
									<img src="{{asset('assets/img/tabungan.svg')}}" class="icon-emas" style="width: 58.56px; height: 62.4px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
									<p class="z-tabungan">Zakat Tabungan</p>
									<p class="z-des-penghasilan">Muslim yang memiliki tabungan dan terhitung mencapai satu tahun dan nilainya setara dengan 85gr emas, maka wajib mengeluarkan zakat. </p>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-perdagangan">
							<div class="card-2">
								<i class="fas fa-shopping-bag fa-3x" style="color: #FFA825; margin-left: 16px; margin-top: 12px;"></i>
								<p class="z-perdagangan">Zakat Perdagangan</p>
								<p class="z-des-penghasilan">Zakat harta yang dikeluarkan dari hasil pendapatan seseorang atau profesinya bila telah mencapai nisab</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#perak">
							<div class="card-6">
								<div class="clearfix">
									<img src="{{asset('assets/img/emas.svg')}}" class="icon-emas" style="width: 56px; height: 46.8px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
									<p class="z-perak">Zakat Perak</p>
									<p class="z-des-penghasilan">simpanan perak selama satu tahun dan mencapai 595 gram perak, maka wajib mengeluarkan zakat sebanyak 2,5 persen.</p>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#emas-perak">
							<div class="card-3">
								<div class="clearfix">
									<img src="{{asset('assets/img/emas.svg')}}" class="icon-emas" style="width: 56px; height: 46.8px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
									<p class="emas-perak">Zakat Emas</p>
									<p class="z-des-penghasilan">Setiap muslim yang memiliki simpanan emas selama satu tahun dan mencapai 85 gram, maka wajib mengeluarkan zakat sebanyak 2,5 persen.</p>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-5 mt-2">
					<div class="card-body">
						<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-pertanian">
							<div class="card-4">
								<div class="clearfix">
									<img src="{{asset('assets/img/pertanian.svg')}}" style="float: left; color: #FFA825; margin-left: 20px; margin-top: 0px;">
									<p class="z-pertanian">Zakat Pertanian</p>
									<p class="z-des-penghasilan">Hasil pertanian dengan kadar 5% jika dengan irigasi (mengeluarkan biaya) atau 10% dengan perairan alami (tidak mengeluarkan biaya).</p>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="mt-5">
		<img src="{{asset('assets/img/bolakiri.png')}}" width="200" class="float-left img-responsive">
		<img src="{{asset('assets/img/bolakanan.png')}}" width="184" class="float-right img-responsive">
	</footer>

	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet" />

	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-180x180.png" />
	<style type="text/css">
		.datepicker {
			font-size: 0.875em;
		}

		.datepicker td,
		.datepicker th {
			width: 1.5em;
			height: 1.5em;
		}
	</style>
	</head>

	<body>
		<div class="container">
			<div class="home">
				<div class="text-center">
					<a href="https://zakatsukses.org/"><img src="{{asset('assets/img/logo.png')}}" class="image-logo" width="150px"></a>
				</div>
				<div class="text-center mt-4">
					<p class="judul-form">Kalkulator Perhitungan Zakat</p>
				</div>

				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-penghasilan">
								<div class="card-1">
									<i class="fas fa-wallet fa-3x" style="color: #FFA825; margin-left: 16px; margin-top: 12px;"></i>
									<p class="z-penghasilan" style="text-decoration:none">Zakat Penghasilan</p>
									<p class="z-des-penghasilan">Zakat harta yang dikeluarkan dari hasil pendapatan seseorang atau profesinya bila telah mencapai nisab</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-tabungan">
								<div class="card-5">
									<div class="clearfix">
										<img src="{{asset('assets/img/tabungan.svg')}}" class="icon-emas" style="width: 58.56px; height: 62.4px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
										<p class="z-tabungan">Zakat Tabungan</p>
										<p class="z-des-penghasilan">Muslim yang memiliki tabungan dan terhitung mencapai satu tahun dan nilainya setara dengan 85gr emas, maka wajib mengeluarkan zakat. </p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-perdagangan">
								<div class="card-2">
									<i class="fas fa-shopping-bag fa-3x" style="color: #FFA825; margin-left: 16px; margin-top: 12px;"></i>
									<p class="z-perdagangan">Zakat Perdagangan</p>
									<p class="z-des-penghasilan">Zakat harta yang dikeluarkan dari hasil pendapatan seseorang atau profesinya bila telah mencapai nisab</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#perak">
								<div class="card-6">
									<div class="clearfix">
										<img src="{{asset('assets/img/emas.svg')}}" class="icon-emas" style="width: 56px; height: 46.8px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
										<p class="z-perak">Zakat Perak</p>
										<p class="z-des-penghasilan">simpanan perak selama satu tahun dan mencapai 595 gram perak, maka wajib mengeluarkan zakat sebanyak 2,5 persen.</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#emas-perak">
								<div class="card-3">
									<div class="clearfix">
										<img src="{{asset('assets/img/emas.svg')}}" class="icon-emas" style="width: 56px; height: 46.8px; float: left; color: #FBB540; margin-left: 10px; margin-top: 0px;">
										<p class="emas-perak">Zakat Emas</p>
										<p class="z-des-penghasilan">Setiap muslim yang memiliki simpanan emas selama satu tahun dan mencapai 85 gram, maka wajib mengeluarkan zakat sebanyak 2,5 persen.</p>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-sm-5 mt-2">
						<div class="card-body">
							<a href="" style="text-decoration: none;" data-toggle="modal" data-target="#z-pertanian">
								<div class="card-4">
									<div class="clearfix">
										<img src="{{asset('assets/img/pertanian.svg')}}" style="float: left; color: #FFA825; margin-left: 20px; margin-top: 0px;">
										<p class="z-pertanian">Zakat Pertanian</p>
										<p class="z-des-penghasilan">Hasil pertanian dengan kadar 5% jika dengan irigasi (mengeluarkan biaya) atau 10% dengan perairan alami (tidak mengeluarkan biaya).</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="mt-5">
			<img src="{{asset('assets/img/bolakiri.png')}}" width="200" class="float-left img-responsive">
			<img src="{{asset('assets/img/bolakanan.png')}}" width="184" class="float-right img-responsive">
		</footer>
		{{-- Form Modal --}}
		@include('form-modal.modal')

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script src="{{asset('assets/js/accounting.js')}}"></script>

		<script src="{{asset('assets/js/hitung-zakat.js')}}"></script>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

		<script type="text/javascript">
			$(function() {
				$('#tgl_simpan').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy"
				});

				$('#tgl_haul').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy"
				});
			});
		</script>

		<script type="text/javascript">
			$(function() {
				$('#simpan-perak').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy"
				});

				$('#haul-perak').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy"
				});
			});
		</script>

	</body>

</html>