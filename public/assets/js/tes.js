// Emas
function perak() {
    var simpanp = document.getElementById('simpanp-perak').value;
    simpanp = new Date('"' + simpanp + '"');
    var haulp = document.getElementById('haulp_perak').value;
    haulp = new Date('"' + haulp + '"')
    var jarakp = haulp.getTime() - simpanp.getTime();
    jarakp = jarakp / (1000 * 3600 * 24);
    if (jarakp >= 365) {
        var jumPerak = document.getElementById('jumlah-perak').value;
        var hargap = jumPerak.replace(/[.]+/g, '');
        console.log(hargap);

        if (hargap >= 85) {
            var zakatperak = hargap * 924000;
            var jumlahZakatP = (((zakatperak) * 2.5) / 100);
            $('#wajibperak').attr('value', 'YA');
            $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahZakatP, "0", ".")));
            // Buttonp
            var buttonpp = document.querySelector('#btn-perak');
            buttonpp.style.opacity = 1;
            buttonpp.style.pointerEvents = "unset";
        }
    } else {
        $('#wajibperak').attr('value', 'Tidak');
        $('#jumlah-zakat-perak').attr('value', 0);
        // Buttonpp
        var buttonpp = document.querySelector('#btn-perak');
        buttonpp.style.opacity = 0.6;
        buttonpp.style.pointerEvents = "none";
    }
}

var jumPerak = document.getElementById('jumlah-perak');
jumPerak.addEventListener('keyup', function () {
    jumPerak.value = formatRupiah(this.value, '');
    var hargap = jumPerak.value.replace(/[.]+/g, '');

    if (hargap >= 85) {
        var simpanp = document.getElementById('simpanp-perak').value;
        simpanp = new Date('"' + simpanp + '"');
        var haulp = document.getElementById('haulp_perak').value;
        haulp = new Date('"' + haulp + '"')
        var jarakp = haulp.getTime() - simpanp.getTime();
        jarakp = jarakp / (1000 * 3600 * 24);

        if (jarakp >= 365) {
            var zakatperak = hargap * 924000;
            var jumlahZakatP = (((zakatperak) * 2.5) / 100);
            $('#wajibperak').attr('value', 'YA');
            $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahZakatP, "0", ".")));
            // Buttonp
            var buttonp = document.querySelector('#btn-perak');
            buttonp.style.opacity = 1;
            buttonp.style.pointerEvents = "unset";
        } else {
            $('#wajibperak').attr('value', 'Tidak');
            $('#jumlah-zakat-perak').attr('value', 0);
            // Buttonp
            var buttonp = document.querySelector('#btn-perak');
            buttonp.style.opacity = 0.6;
            buttonp.style.pointerEvents = "none";
        }
    } else {
        $('#wajibperak').attr('value', 'Tidak');
        $('#jumlah-zakat-perak').attr('value', 0);
        // Buttonp
        var buttonp = document.querySelector('#btn-perak');
        buttonp.style.opacity = 0.6;
        buttonp.style.pointerEvents = "none";
    }
})
