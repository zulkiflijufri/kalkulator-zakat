// Penghasilan
var penghasilan = document.getElementById('peng-per');
penghasilan.addEventListener('keyup', function (e) {
    penghasilan.value = formatRupiah(this.value, '');
    var peng = penghasilan.value.replace(/[.]+/g, '');
    var pengeluaran = document.getElementById('pengeluaran').value;
    pengeluaran = pengeluaran.replace(/[.]+/g, '');

    if (peng >= 5877000) {
        if (pengeluaran == '') {
            var pengPb = ((peng * 2.5) / 100);
            $('#wajibjzakat').attr('value', 'YA');
            $('#peng-pb').attr('value', (accounting.formatNumber(pengPb, "0", ".")));
        } else {
            var hasil = peng - pengeluaran;
            if (hasil < 5877000) {
                $('#wajibjzakat').attr('value', 'Tidak');
                $('#peng-pb').attr('value', 0);
            } else {
                if (peng >= 5877000) {
                    var pengPb = ((hasil * 2.5) / 100);
                    $('#wajibjzakat').attr('value', 'YA');
                    $('#peng-pb').attr('value', (accounting.formatNumber(pengPb, "0", ".")));
                }
            }
        }
    } else {
        $('#wajibjzakat').attr('value', 'Tidak');
        $('#peng-pb').attr('value', 0);
        $('#peng-pt').attr('value', 0);
    }
    // Button
    var per = document.getElementById('peng-pb');
    if (per.value > "0") {
        var button = document.querySelector('#btn-penghasilan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-penghasilan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
})

var pengeluaran = document.getElementById('pengeluaran');
pengeluaran.addEventListener('keyup', function (e) {
    pengeluaran.value = formatRupiah(this.value, '');
    var fix_pengeluaran = pengeluaran.value.replace(/[.]+/g, '');
    var pengPer = document.getElementById('peng-per');
    var penghasilan = pengPer.value.replace(/[.]+/g, '');

    if (penghasilan >= 5877000) {
        var hasil = penghasilan - fix_pengeluaran;
        if (hasil < 5877000) {
            $('#wajibjzakat').attr('value', 'Tidak');
            $('#peng-pb').attr('value', 0);
        } else {
            var pengPb = ((hasil * 2.5) / 100);
            $('#wajibjzakat').attr('value', 'YA');
            $('#peng-pb').attr('value', (accounting.formatNumber(pengPb, "0", ".")));
        }
    } else {
        $('#wajibjzakat').attr('value', 'Tidak');
        $('#peng-pb').attr('value', 0);
    }
    // Button
    var per = document.getElementById('peng-pb');
    if (per.value > "0") {
        var button = document.querySelector('#btn-penghasilan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-penghasilan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
});

// Tabungan
var salBag = document.getElementById('sal-bag');
salBag.addEventListener('keyup', function () {
    salBag.value = formatRupiah(this.value, '');
    var tabungan = salBag.value.replace(/[.]+/g, '');

    if (tabungan >= 78540000) {
        var jumlahZakat = (((tabungan) * 2.5) / 100);
        $('#wajibtzakat').attr('value', 'YA');
        $('#jumlah-zakat').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
    } else {
        $('#wajibtzakat').attr('value', 'Tidak');
        $('#jumlah-zakat').attr('value', 0);
    }
    // Button
    var per = document.getElementById('jumlah-zakat');
    if (per.value > "0") {
        var button = document.querySelector('#btn-tabungan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-tabungan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
});

// Emas
function tanggal() {
    var simpan = document.getElementById('tgl_simpan').value;
    simpan = new Date('"' + simpan + '"');
    var haul = document.getElementById('tgl_haul').value;
    haul = new Date('"' + haul + '"')
    var jarak = haul.getTime() - simpan.getTime();
    jarak = jarak / (1000 * 3600 * 24);
    if (jarak >= 365) {
        var jumEmas = document.getElementById('jumlah-emas').value;
        var hargae = jumEmas.replace(/[.]+/g, '');
        console.log(hargae);

        if (hargae >= 85) {
            var zakatemas = hargae * 924000;
            var jumlahZakat = (((zakatemas) * 2.5) / 100);
            $('#wajibezakat').attr('value', 'YA');
            $('#jumlah-zakat-emas').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
            // Button
            var button = document.querySelector('#btn-emas');
            button.style.opacity = 1;
            button.style.pointerEvents = "unset";
        }
    } else {
        $('#wajibezakat').attr('value', 'Tidak');
        $('#jumlah-zakat-emas').attr('value', 0);
        // Button
        var button = document.querySelector('#btn-emas');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
}

var jumEmas = document.getElementById('jumlah-emas');
jumEmas.addEventListener('keyup', function () {
    jumEmas.value = formatRupiah(this.value, '');
    var hargae = jumEmas.value.replace(/[.]+/g, '');

    if (hargae >= 85) {
        var simpan = document.getElementById('tgl_simpan').value;
        simpan = new Date('"' + simpan + '"');
        var haul = document.getElementById('tgl_haul').value;
        haul = new Date('"' + haul + '"')
        var jarak = haul.getTime() - simpan.getTime();
        jarak = jarak / (1000 * 3600 * 24);

        if (jarak >= 365) {
            var zakatemas = hargae * 924000;
            var jumlahZakat = (((zakatemas) * 2.5) / 100);
            $('#wajibezakat').attr('value', 'YA');
            $('#jumlah-zakat-emas').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
            // Button
            var button = document.querySelector('#btn-emas');
            button.style.opacity = 1;
            button.style.pointerEvents = "unset";
        } else {
            $('#wajibezakat').attr('value', 'Tidak');
            $('#jumlah-zakat-emas').attr('value', 0);
            // Button
            var button = document.querySelector('#btn-emas');
            button.style.opacity = 0.6;
            button.style.pointerEvents = "none";
        }
    } else {
        $('#wajibezakat').attr('value', 'Tidak');
        $('#jumlah-zakat-emas').attr('value', 0);
        // Button
        var button = document.querySelector('#btn-emas');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
})

// Perak
// var jumPerak = document.getElementById('jumlah-perak');
// jumPerak.addEventListener('keyup', function () {
//     jumPerak.value = formatRupiah(this.value, '');
//     var hargaa = jumPerak.value.replace(/[.]+/g, '');
//     if (hargaa >= 595) {
//         var zakatperak = hargaa * 8200;
//         var jumlahPerak = (((zakatperak) * 2.5) / 100);
//         $('#wajibperak').attr('value', 'YA');
//         $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahPerak, "0", ".")));
//     } else {
//         $('#wajibperak').attr('value', 'Tidak');
//         $('#jumlah-zakat-perak').attr('value', 0);
//     }
//     // Button
//     var per = document.getElementById('jumlah-zakat-perak');
//     if (per.value > "0") {
//         var button = document.querySelector('#btn-perak');
//         button.style.opacity = 1;
//         button.style.pointerEvents = "unset";
//     }
// });

// Perak
function perak() {
    var simpanp = document.getElementById('simpan-perak').value;
    simpanp = new Date('"' + simpanp + '"');
    var haulp = document.getElementById('haul-perak').value;
    haulp = new Date('"' + haulp + '"')
    var jarakp = haulp.getTime() - simpanp.getTime();
    jarakp = jarakp / (1000 * 3600 * 24);
    if (jarakp >= 365) {
        var jumPerak = document.getElementById('jumlah-perak').value;
        var hargap = jumPerak.replace(/[.]+/g, '');
        console.log(hargap);

        if (hargap >= 85) {
            var zakatperak = hargap * 8200;
            var jumlahZakatP = (((zakatperak) * 2.5) / 100);
            $('#wajibperak').attr('value', 'YA');
            $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahZakatP, "0", ".")));
            // Buttonp
            var buttonpp = document.querySelector('#btn-perak');
            buttonpp.style.opacity = 1;
            buttonpp.style.pointerEvents = "unset";
        }
    } else {
        $('#wajibperak').attr('value', 'Tidak');
        $('#jumlah-zakat-perak').attr('value', 0);
        // Buttonpp
        var buttonpp = document.querySelector('#btn-perak');
        buttonpp.style.opacity = 0.6;
        buttonpp.style.pointerEvents = "none";
    }
}

var jumPerak = document.getElementById('jumlah-perak');
jumPerak.addEventListener('keyup', function () {
    jumPerak.value = formatRupiah(this.value, '');
    var hargap = jumPerak.value.replace(/[.]+/g, '');

    if (hargap >= 85) {
        var simpanp = document.getElementById('simpan-perak').value;
        simpanp = new Date('"' + simpanp + '"');
        var haulp = document.getElementById('haul-perak').value;
        haulp = new Date('"' + haulp + '"')
        var jarakp = haulp.getTime() - simpanp.getTime();
        jarakp = jarakp / (1000 * 3600 * 24);

        if (jarakp >= 365) {
            var zakatperak = hargap * 8200;
            var jumlahZakatP = (((zakatperak) * 2.5) / 100);
            $('#wajibperak').attr('value', 'YA');
            $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahZakatP, "0", ".")));
            // Buttonp
            var buttonp = document.querySelector('#btn-perak');
            buttonp.style.opacity = 1;
            buttonp.style.pointerEvents = "unset";
        } else {
            $('#wajibperak').attr('value', 'Tidak');
            $('#jumlah-zakat-perak').attr('value', 0);
            // Buttonp
            var buttonp = document.querySelector('#btn-perak');
            buttonp.style.opacity = 0.6;
            buttonp.style.pointerEvents = "none";
        }
    } else {
        $('#wajibperak').attr('value', 'Tidak');
        $('#jumlah-zakat-perak').attr('value', 0);
        // Buttonp
        var buttonp = document.querySelector('#btn-perak');
        buttonp.style.opacity = 0.6;
        buttonp.style.pointerEvents = "none";
    }
})


//Perdagangan
var modal = document.getElementById('dmodal');
modal.addEventListener('keyup', function () {
    modal.value = formatRupiah(this.value, '');
    modalper = modal.value.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (modalper >= 78540000) {
        if ((dkeuntungan == '') && (dpiutang == '') && (dhutang == '')) {
            var zakatPer = ((modalper * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
})

var dkeuntungan = document.getElementById('dkeuntungan');
dkeuntungan.addEventListener('keyup', function () {
    dkeuntungan.value = formatRupiah(this.value, '');
    keuntunganper = dkeuntungan.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    modal = modal.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (keuntunganper >= 78540000) {
        if ((modal == '') && (dpiutang == '') && (dhutang == '')) {
            var zakatPer = ((keuntunganper * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else if (keuntunganper < 78540000) {
        if (modal != '') {
            keuntunganper = parseInt(modal) + parseInt(keuntunganper);
            if (keuntunganper >= 78540000) {
                var zakatPer = ((keuntunganper * 2.5) / 100);
                $('#zdwajib').attr('value', 'YA');
                $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            } else {
                $('#zdwajib').attr('value', 'Tidak');
                $('#zdagang').attr('value', 0);
            }
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }

})

var dpiutang = document.getElementById('dpiutang');
dpiutang.addEventListener('keyup', function () {
    dpiutang.value = formatRupiah(this.value, '');
    piutang = dpiutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (piutang >= 78540000) {
        if ((modal == '') && (dkeuntungan == '') && (dhutang == '')) {
            var zakatPer = ((piutang * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else if (piutang < 78540000) {
        if ((modal != '') && (dkeuntungan != '')) {
            piutang = parseInt(modal) + parseInt(dkeuntungan) + parseInt(piutang);
            if (piutang >= 78540000) {
                var zakatPer = ((piutang * 2.5) / 100);
                $('#zdwajib').attr('value', 'YA');
                $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            } else {
                $('#zdwajib').attr('value', 'Tidak');
                $('#zdagang').attr('value', 0);
            }
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }

})

var dhutang = document.getElementById('dhutang');
dhutang.addEventListener('keyup', function () {
    dhutang.value = formatRupiah(this.value, '');
    hutang = dhutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    console.log(modal);
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');

})

var dhutang = document.getElementById('dhutang');
dhutang.addEventListener('keyup', function () {
    dhutang.value = formatRupiah(this.value, '');
    hutang = dhutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    console.log(modal);
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');

    if ((modal != '') && (dkeuntungan != '') && (dpiutang != '')) {
        hutang = parseInt(modal) + parseInt(dkeuntungan) + parseInt(dpiutang) - hutang;
        if (hutang >= 78540000) {
            var zakatPer = ((hutang * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        } else {
            $('#zdwajib').attr('value', 'Tidak');
            $('#zdagang').attr('value', 0);
        }
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }


})

// Pertanian
var pertanian = document.getElementById('zakat-per');
pertanian.addEventListener('keyup', function () {
    pertanian.value = formatRupiah(this.value, '');
    var harga = pertanian.value.replace(/[.]+/g, '');
    var pilihan = document.getElementById('pilihan');
    pilihan = pilihan.options[pilihan.selectedIndex].value;

    if (harga >= 5877000) {
        if (pilihan == 'irigasi') {
            var zakatPer = ((harga * 5) / 100);
            $('#jum-per').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            $('#wajib-per').attr('value', 'Ya');
        } else if (pilihan == 'tadah_hujan') {
            var zakatPer = ((harga * 10) / 100);
            $('#jum-per').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            $('#wajib-per').attr('value', 'Ya');
        } else {
            $('#wajib-per').attr('value', 'Ya');
        }
    } else {
        $('#wajib-per').attr('value', 'Tidak');
        $('#jum-per').attr('value', 0);
    }
    // Button
    var per = document.getElementById('jum-per');
    if (per.value > "0") {
        var button = document.querySelector('#btn-pertanian');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-pertanian');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
});

function pilih() {
    var pilihan = document.getElementById('pilihan');
    pilihan = pilihan.options[pilihan.selectedIndex].value;
    var pertanian = document.getElementById('zakat-per');
    var harga = pertanian.value.replace(/[.]+/g, '');

    if (pilihan == 'irigasi') {
        if (harga >= 5877000) {
            var zakatPer = ((harga * 5) / 100);
            $('#jum-per').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else {
        if (harga >= 5877000) {
            var zakatPer = ((harga * 10) / 100);
            $('#jum-per').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    }
    // Button
    var per = document.getElementById('jum-per');
    if (per.value > "0") {
        var button = document.querySelector('#btn-pertanian');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-pertanian');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
}

function formatRupiah(angka, prefix) {
    var number = angka.replace(/[^\d]/g, '').toString();
    split = number.split(',');
    sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
}
