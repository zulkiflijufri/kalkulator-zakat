//Perdagangan
var modal = document.getElementById('dmodal');
modal.addEventListener('keyup', function () {
    modal.value = formatRupiah(this.value, '');
    modalper = modal.value.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (modalper >= 78540000) {
        if ((dkeuntungan == '') && (dpiutang == '') && (dhutang == '')) {
            var zakatPer = ((modalper * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }
})

var dkeuntungan = document.getElementById('dkeuntungan');
dkeuntungan.addEventListener('keyup', function () {
    dkeuntungan.value = formatRupiah(this.value, '');
    keuntunganper = dkeuntungan.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    modal = modal.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (keuntunganper >= 78540000) {
        if ((modal == '') && (dpiutang == '') && (dhutang == '')) {
            var zakatPer = ((keuntunganper * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else if (keuntunganper < 78540000) {
        if (modal != '') {
            keuntunganper = parseInt(modal) + parseInt(keuntunganper);
            if (keuntunganper >= 78540000) {
                var zakatPer = ((keuntunganper * 2.5) / 100);
                $('#zdwajib').attr('value', 'YA');
                $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            } else {
                $('#zdwajib').attr('value', 'Tidak');
                $('#zdagang').attr('value', 0);
            }
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }

})

var dpiutang = document.getElementById('dpiutang');
dpiutang.addEventListener('keyup', function () {
    dpiutang.value = formatRupiah(this.value, '');
    piutang = dpiutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dhutang = document.getElementById('dhutang').value;
    dhutang = dhutang.replace(/[.]+/g, '');

    if (piutang >= 78540000) {
        if ((modal == '') && (dkeuntungan == '') && (dhutang == '')) {
            var zakatPer = ((piutang * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        }
    } else if (piutang < 78540000) {
        if ((modal != '') && (dkeuntungan != '')) {
            piutang = parseInt(modal) + parseInt(dkeuntungan) + parseInt(piutang);
            if (piutang >= 78540000) {
                var zakatPer = ((piutang * 2.5) / 100);
                $('#zdwajib').attr('value', 'YA');
                $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
            } else {
                $('#zdwajib').attr('value', 'Tidak');
                $('#zdagang').attr('value', 0);
            }
        }
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }

})

var dhutang = document.getElementById('dhutang');
dhutang.addEventListener('keyup', function () {
    dhutang.value = formatRupiah(this.value, '');
    hutang = dhutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    console.log(modal);
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');

})

var dhutang = document.getElementById('dhutang');
dhutang.addEventListener('keyup', function () {
    dhutang.value = formatRupiah(this.value, '');
    hutang = dhutang.value.replace(/[.]+/g, '');
    var modal = document.getElementById('dmodal').value;
    console.log(modal);
    modal = modal.replace(/[.]+/g, '');
    var dkeuntungan = document.getElementById('dkeuntungan').value;
    dkeuntungan = dkeuntungan.replace(/[.]+/g, '');
    var dpiutang = document.getElementById('dpiutang').value;
    dpiutang = dpiutang.replace(/[.]+/g, '');

    if ((modal != '') && (dkeuntungan != '') && (dpiutang != '')) {
        hutang = parseInt(modal) + parseInt(dkeuntungan) + parseInt(dpiutang) - hutang;
        if (hutang >= 78540000) {
            var zakatPer = ((hutang * 2.5) / 100);
            $('#zdwajib').attr('value', 'YA');
            $('#zdagang').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
        } else {
            $('#zdwajib').attr('value', 'Tidak');
            $('#zdagang').attr('value', 0);
        }
    }

    // Button
    var per = document.getElementById('zdagang');
    if (per.value > "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 1;
        button.style.pointerEvents = "unset";
    } else if (per.value == "0") {
        var button = document.querySelector('#btn-perdagangan');
        button.style.opacity = 0.6;
        button.style.pointerEvents = "none";
    }


})
