<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorZakat extends Controller
{
    public function form()
    {
        $method = request()->method();
        $querystr = request()->getRequestUri();

		if ($querystr != "/form-donasi") {
			return redirect(route('form.donasi'));
		}
		return view('form',compact('querystr','method'));
	}

    public function formDonasi()
    {
        $data = request()->all();
        $method = request()->method();
        return view('form', compact('data', 'method'));
    }

    public function transfer()
    {
        $data = request()->all();
        $method = request()->method();
        return view('transfer', compact('data', 'method'));
    }
}
