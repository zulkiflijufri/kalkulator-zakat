<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Form Donasi</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">

	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://zakatsukses.org/wp-content/uploads/2019/06/cropped-ZS-Bulet-180x180.png" />

</head>

<body>
	<div class="home">
		<div class="text-center">
			<a href="https://zakatsukses.org/"><img src="<?php echo e(asset('assets/img/logo.png')); ?>" class="image-logo" width="150px"></a>
		</div>
		<div class="judul-form text-center">
			<p>Data Donasi</p>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-10 mt-5">
					<form>
						<div class="form-group row">
							<label for="nama_lengkap" class="col-sm-4 col-form-label nama-lengkap">Nama Lengkap</label>
							<div class="col-sm-8">
								<input type="text" class="form-control form-nama" id="nama_lengkap" placeholder="Nama Lengkap Anda" autofocus autocomplete="off" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="atas_nama" class="col-sm-4 col-form-label atas-nama">Atas Nama</label>
							<div class="col-sm-8">
								<input type="text" class="form-control form-atas-nama" id="atas_nama" placeholder="Atas Nama" required autocomplete="off">
							</div>
						</div>

						<div class="form-group row">
							<label for="jenis_kelamin" class="col-sm-4 col-form-label jenis-kelamin">Jenis Kelamin</label>
							<div class="col-sm-8">
								<select class="form-control form-jenis-kelamin" id="jenis_kelamin" required>
									<option selected hidden value="">Pilih</option>
									<option value="L" class="pil-jk">Laki-Laki</option>
									<option value="P" class="pil-jk">Perempuan</option>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label for="email" class="col-sm-4 col-form-label email">Email</label>
							<div class="col-sm-8">
								<input type="email" class="form-control form-email" id="email" placeholder="Email" required autocomplete="off">
							</div>
						</div>

						<div class="form-group row">
							<label for="phone" class="col-sm-4 col-form-label phone">Phone</label>
							<div class="col-sm-8">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">+62</span>
									</div>
									<input type="text" class="form-control" id="phone" maxlength="12" required autocomplete="off">
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label for="akad_donasi" class="col-sm-4 col-form-label akad-donasi">Akad Donasi</label>
							<div class="col-sm-8">
								<?php if($method == "GET"): ?>
								<select class="form-control" id="akad_donasi" disabled>
									<option value="Zakat Penghasilan" selected>Zakat Penghasilan</option>
								</select>
								<?php endif; ?>

								<?php if($method == "POST"): ?>
								<select class="form-control" id="akad_donasi" disabled>
									<option value="Zakat Penghasilan" <?php if($data['zakat']=='penghasilan' ): ?> selected <?php endif; ?>>Zakat Penghasilan</option>
									<option value="Zakat Tabungan" <?php if($data['zakat']=='tabungan' ): ?> selected <?php endif; ?>>Zakat Tabungan</option>
									<option value="Zakat Perdagangan" <?php if($data['zakat']=='perdagangan' ): ?> selected <?php endif; ?>>Zakat Perdagangan</option>
									<option value="Zakat Perak" <?php if($data['zakat']=='perak' ): ?> selected <?php endif; ?>>Zakat Perak</option>
									<option value="Zakat Emas" <?php if($data['zakat']=='emas' ): ?> selected <?php endif; ?>>Zakat Emas</option>
									<option value="Zakat Pertanian" <?php if($data['zakat']=='pertanian' ): ?> selected <?php endif; ?>>Zakat Pertanian</option>
								</select>
								<?php endif; ?>
							</div>
						</div>

						<div class="form-group row">
							<label for="nominal" class="col-sm-4 col-form-label nominal">Nominal</label>
							<div class="input-group col-sm-8 form-nominal">
								<div class="input-group-prepend pb-4">
									<span class="input-group-text" id="basic-addon1">Rp</span>
								</div>
								<?php if($method == 'GET'): ?>
								<input type="text" class="form-control" id="nominal" autocomplete="off" required>
								<?php endif; ?>
								<?php if(($method == "POST") && ($data['zakat'] == "penghasilan")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['penghasilan']); ?>">
								<?php elseif(($method == "POST") && ($data['zakat'] == "tabungan")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['tabungan']); ?>">
								<?php elseif(($method == "POST") && ($data['zakat'] == "perdagangan")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['perdagangan']); ?>">
								<?php elseif(($method == "POST") && ($data['zakat'] == "emas")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['emas']); ?>">
								<?php elseif(($method == "POST") && ($data['zakat'] == "perak")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['perak']); ?>">
								<?php elseif(($method == "POST") && ($data['zakat'] == "pertanian")): ?>
								<input type="text" class="form-control" id="nominal" value="<?php echo e($data['pertanian']); ?>">
								<?php endif; ?>
							</div>
						</div>

						<div class="form-group row">
							<label for="bank" class="col-sm-4 col-form-label bank">Bank</label>
							<div class="col-sm-8">
								<select class="form-control form-bank" id="bank" required onchange="changeLink();">
									<option hidden value="">Pilih Jenis Bank</option>
									<option>Bank Bjb</option>
									<option>Bank DKI</option>
									<option>Bank Mega Syariah</option>
									<option>Bank Muamalat</option>
									<option>BRI Syariah</option>
									<option>BNI Syariah</option>
									<option>Mandiri Syariah</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="row text-center mt-2">
								<div class="col-sm-4"></div>
								<div class="col-sm-8 text-center mt-3">
									<a href="#" id="donasi" class="btn btn-md button" style="width: 50%;" target="_blank">Donasi</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer class="mt-5">
		<img src="<?php echo e(asset('assets/img/bolakiri.png')); ?>" width="200" class="float-left img-responsive">
		<img src="<?php echo e(asset('assets/img/bolakanan.png')); ?>" width="184" class="float-right img-responsive">
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function changeLink() {
			// var nama_lengkap = document.getElementById('nama_lengkap').value;
			var nama = document.getElementById('atas_nama').value;
			// var jenis_kelamin = document.getElementById('jenis_kelamin').value;
			// var email = document.getElementById('email').value;
			// var phone = document.getElementById('phone').value;
			var akad = document.getElementById('akad_donasi').value;
			var nominal = document.getElementById('nominal').value;
			var bank = document.getElementById('bank');
			bank = bank.options[bank.selectedIndex].text;
			var link = document.getElementById('donasi');

			if (bank == "Bank Bjb") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else if (bank == "Bank DKI") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else if (bank == "Bank Mega Syariah") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else if (bank == "Bank Muamalat") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else if (bank == "BRI Syariah") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else if (bank == "BNI Syariah") {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			} else {
				link.setAttribute('href', "https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20" + nama + "%2C%20mau%20mentransfer%20" + akad + "%20senilai%20Rp.%20" + nominal + "%20melalui%20rekening%20" + bank + ".%0A%0ATerima%20Kasih.");
			}
		}

		var nominal = document.getElementById('nominal');
		nominal.addEventListener('keyup', function() {
			nominal.value = formatRupiah(this.value, '');
		});

		var phone = document.getElementById('phone');
		phone.addEventListener('keyup', function() {
			phone.value = formatPhone(this.value, '');
			phone.value.replace(/[.]+/g, '');
		});

		function formatRupiah(angka, prefix) {
			var number = angka.replace(/[^\d]/g, '').toString();
			split = number.split(',');
			sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
		}

		function formatPhone(phone, prefix) {
			var number = phone.replace(/[^\d]/g, '').toString();
			split = number.split(',');
			number = split[0].substr(0);
			return prefix == undefined ? number : (number ? '' + number : '');
		}
	</script>
</body>

</html>



<!-- numpang --><?php /**PATH /opt/lampp/htdocs/kalkulator-zakat/resources/views/form.blade.php ENDPATH**/ ?>